<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>


## My notes
- {+ How to build Laravel UI with +} [Bootstrap](https://dev.to/suleman/log-in-registration-with-laravelui-in-laravel-5ddk) , and remember remove ` "vite" ` related tags in the blade templates, then add URL for bootstrap CDN URL.

- [- Change -] [Login with USERNAME instead of EMAIL](https://dev.to/shanisingh03/how-to-login-with-username-instead-of-email-in-laravel--hj8)

- {+ The Blog and images establishment is base on the +} [article](https://www.fundaofwebit.com/post/laravel-10-multiple-image-upload-with-example)



## License
The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
